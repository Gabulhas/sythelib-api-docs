# Animation

## /animation

Fetch information of player animation and pose.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
|animation| number | Current animation of the player (Idle: -1)|
|pose | number | Current pose of the player |
|poseIdle | number | pose of player if it's idle|


### Example Response

```json

{
    "animation": 623,
    "pose": 808,
    "poseIdle": 808
}

```