# Varplayer

## /varplayer

Fetch value of varplayer.

### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| varplayer | number | number of the Varplayer|

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
|value| number | Value of the Varplayer |


### Example Request
`/varplayer?varplayer=2239`


### Example Response
```json
{
 "value": 100 
}
```