
# Varbit

## /varbit

Fetch value of varbit.

### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| varbit | number | number of the Varbit|

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
|value| number | Value of the Varbit |


### Example Request
`/varbit?varbit=2323`


### Example Response
```json
{
 "value": 45

}
```