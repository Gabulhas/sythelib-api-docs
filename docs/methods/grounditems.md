
# Ground Items

## /grounditems

Fetch ground items.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [GroundItem](/sythelib-api-docs/objects/grounditem/) Array| Array containing all the [GroundItem](/sythelib-api-docs/objects/grounditem/)s loaded.|



### Example Response

```json

{
    "animation": 623,
    "pose": 808,
    "poseIdle": 808
}

```


## /grounditems/neareast

Fetch the closes Ground item to a Point. If no Point is defined it will return the nearest to the Player.

### Input parameters (optional)
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| x | number | x coordinate of the Tile|
| y | number | y coordinate of the Tile|
| z | number | z coordinate of the Tile|


### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [GroundItem](/sythelib-api-docs/objects/grounditem/) | [GroundItem](/sythelib-api-docs/objects/grounditem/)s nearest to player.|

### Example Request
`/grounditems/nearest?x=2500&y=3510`


### Example Response

```json
{
    "id": 592,
    "quantity": 1,
    "name": "Ashes",
    "pos": {
        "x": 3167,
        "y": 3496,
        "z": 0
    },
    "canvas": {
        "polys": [
            {
                "verts": [
                    [
                        121,
                        125
                    ],
                    [
                        122,
                        136
                    ],
                    [
                        123,
                        137
                    ],
                    [
                        139,
                        133
                    ],
                    [
                        139,
                        132
                    ],
                    [
                        137,
                        122
                    ]
                ]
            }
        ]
    }
}
```