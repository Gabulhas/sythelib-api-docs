
# Game Objects

----
All requests can be filtered by ID and/or name, which will only return Entities with such ID and/or name.
### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| id | number | Filter by ID|
| name | string | Filter by name|

### Example Request
`/.../?id=2324&name=Bank booth`

----
## /gameobjects

Fetch all the Game Objects that could be loaded from the player's position.


### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [GameObject](/sythelib-api-docs/objects/gameobject/) Array| Array containing all the [GameObject](/sythelib-api-docs/objects/gameobject/)s loaded.|


### Example Response

```json

[{
        "id": 1982,
        "name": "null",
        "pos": {
            "x": 2459,
            "y": 3512
        }
    },
    {
        "id": 1276,
        "name": "Tree",
        "pos": {
            "x": 2460,
            "y": 3530
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            12,
                            0
                        ],
                        [
                            12,
                            12
                        ],
                        [
                            4,
                            12
                        ],
                        [
                            -11,
                            3
                        ],
                        [
                            -11,
                            -7
                        ],
                        [
                            -11,
                            -7
                        ],
                        [
                            -11,
                            -9
                        ],
                        [
                            -10,
                            -9
                        ],
                        [
                            -10,
                            -11
                        ],
                        [
                            -4,
                            -11
                        ]
                    ]
                }
            ]
        }
    },
    {
        "id": 10820,
        "name": "Oak",
        "pos": {
            "x": 2461,
            "y": 3484
        }
    }]

```


## /gameobjects/nearest_to/player

Fetch the closes object to the Player.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [GameObject](/sythelib-api-docs/objects/gameobject/) | [GameObject](/sythelib-api-docs/objects/gameobject/)s nearest to player.|


### Example Response

```json
    {
        "id": 1276,
        "name": "Tree",
        "pos": {
            "x": 2460,
            "y": 3530
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            12,
                            0
                        ],
                        [
                            12,
                            12
                        ],
                        [
                            4,
                            12
                        ],
                        [
                            -11,
                            3
                        ],
                        [
                            -11,
                            -7
                        ],
                        [
                            -11,
                            -7
                        ],
                        [
                            -11,
                            -9
                        ],
                        [
                            -10,
                            -9
                        ],
                        [
                            -10,
                            -11
                        ],
                        [
                            -4,
                            -11
                        ]
                    ]
                }
            ]
        }
    }
```

## /gameobjects/nearest_to/point

Fetch the closes object to a Point. If no Point is defined it will return the nearest to the Player.


### Input parameters (optional)
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| x | number | x coordinate of the Tile|
| y | number | y coordinate of the Tile|
| z | number | z coordinate of the Tile|


### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [GameObject](/sythelib-api-docs/objects/gameobject/) | [GameObject](/sythelib-api-docs/objects/gameobject/)s nearest to point.|

### Example Request
`/gameobjects/nearest?x=2500&y=3510`

### Example Response

```json
    {
        "id": 1276,
        "name": "Tree",
        "pos": {
            "x": 2499,
            "y": 3509
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            12,
                            0
                        ],
                        [
                            12,
                            12
                        ],
                        [
                            4,
                            12
                        ],
                        [
                            -11,
                            3
                        ],
                        [
                            -11,
                            -7
                        ],
                        [
                            -11,
                            -7
                        ],
                        [
                            -11,
                            -9
                        ],
                        [
                            -10,
                            -9
                        ],
                        [
                            -10,
                            -11
                        ],
                        [
                            -4,
                            -11
                        ]
                    ]
                }
            ]
        }
    }
```