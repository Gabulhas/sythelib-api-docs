# Position

## /Pos

Fetch Player's Coordinates

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
|None | [Position](/sythelib-api-docs/objects/position) Object| Current Position of the player|


### Example Response
```json
{
    "x": 2499,
    "y": 3510,
    "z": 0
}
```