# Tile Scene

## /tilescene

Fetches Position in the minimap of a Tile.

### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| x | number | x coordinate of the Tile|
| y | number | y coordinate of the Tile|
| z | number | z coordinate of the Tile|

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| x | number | X coordinate of the Tile in the minimap|
| y | number | y coordinate of the Tile in the minimap|

### Example Request
`/tileminimap?x=2500&y=3510`

### Example Response
```json
{
    "x": 645,
    "y": 85
}

```

