# Skills

## /skills

Fetch information of Player skills.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
    | Attack | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Defense | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Strength | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Hitpoints | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Ranged | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Prayer | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Magic | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Cooking | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Woodcutting | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Fletching | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Fishing | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Firemaking | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Crafting | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Smithing | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Mining | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Herblore | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Agility | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Thieving | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Slayer | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Farming | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Runecrafting | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Hunter | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |
    | Construction | [Skill](/sythelib-api-docs/objects/skill) Object| Skill information |


### Example Response

```json
{
    "Attack": {
        "level": 50,
        "boostedLevel": 50,
        "xp": 101512.0
    },
    "Defense": {
        "level": 42,
        "boostedLevel": 42,
        "xp": 48928.0
    },
    "Strength": {
        "level": 64,
        "boostedLevel": 64,
        "xp": 412179.0
    },
    "Hitpoints": {
        "level": 51,
        "boostedLevel": 51,
        "xp": 115503.0
    },
    "Ranged": {
        "level": 43,
        "boostedLevel": 43,
        "xp": 54838.0
    },
    "Prayer": {
        "level": 31,
        "boostedLevel": 31,
        "xp": 15660.0
    },
    "Magic": {
        "level": 60,
        "boostedLevel": 60,
        "xp": 286641.0
    },
    "Cooking": {
        "level": 99,
        "boostedLevel": 99,
        "xp": 1.3504765E7
    },
    "Woodcutting": {
        "level": 41,
        "boostedLevel": 41,
        "xp": 45379.0
    },
    "Fletching": {
        "level": 28,
        "boostedLevel": 28,
        "xp": 11605.0
    },
    "Fishing": {
        "level": 85,
        "boostedLevel": 85,
        "xp": 3397356.0
    },
    "Firemaking": {
        "level": 50,
        "boostedLevel": 50,
        "xp": 111261.0
    },
    "Crafting": {
        "level": 43,
        "boostedLevel": 43,
        "xp": 52957.0
    },
    "Smithing": {
        "level": 48,
        "boostedLevel": 48,
        "xp": 85117.0
    },
    "Mining": {
        "level": 52,
        "boostedLevel": 52,
        "xp": 125116.0
    },
    "Herblore": {
        "level": 32,
        "boostedLevel": 32,
        "xp": 17405.0
    },
    "Agility": {
        "level": 64,
        "boostedLevel": 64,
        "xp": 428220.0
    },
    "Thieving": {
        "level": 54,
        "boostedLevel": 54,
        "xp": 161031.0
    },
    "Slayer": {
        "level": 16,
        "boostedLevel": 16,
        "xp": 3000.0
    },
    "Farming": {
        "level": 20,
        "boostedLevel": 20,
        "xp": 4520.0
    },
    "Runecrafting": {
        "level": 14,
        "boostedLevel": 14,
        "xp": 2330.0
    },
    "Hunter": {
        "level": 9,
        "boostedLevel": 9,
        "xp": 1000.0
    },
    "Construction": {
        "level": 14,
        "boostedLevel": 14,
        "xp": 2210.0
    }
}
```