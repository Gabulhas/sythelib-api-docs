
# Menu Entry Swapper

## /addemenuentry
Adds Menu Entry Swap to list.

### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
|target| string | Target of the entry swap|
| option | string | Option of the swap |
| priority | number | Priority of the swap|


## /removemenuentry
Removes Menu Entry Swap from the list.

### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
|target| string | Target of the entry swap|
| option | string | Option of the swap |

## /togglemenuentry
Removes if it's on the list and adds if it's not.

### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
|target| string | Target of the entry swap|
| option | string | Option of the swap |
| priority | number | Priority of the swap|


### Example Request
` /togglemenuentry?priority=100&target=Oak logs&option=drop`
