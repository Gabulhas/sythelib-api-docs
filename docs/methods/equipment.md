
# Equipment

## /equipment

Fetches player equipment.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| head | [Item](/sythelib-api-docs/objects/item/) | Item in head slot |
| cape | [Item](/sythelib-api-docs/objects/item/) | Item in cape slot |
| amulet | [Item](/sythelib-api-docs/objects/item/) | Item in amulet slot |
| weapon | [Item](/sythelib-api-docs/objects/item/) | Item in weapon slot |
| torso | [Item](/sythelib-api-docs/objects/item/) | Item in torso slot |
| shield | [Item](/sythelib-api-docs/objects/item/) | Item in shield slot |
| legs | [Item](/sythelib-api-docs/objects/item/) | Item in legs slot |
| gloves | [Item](/sythelib-api-docs/objects/item/) | Item in gloves slot |
| boots | [Item](/sythelib-api-docs/objects/item/) | Item in boots slot |
| ring | [Item](/sythelib-api-docs/objects/item/) | Item in ring slot |
| ammo | [Item](/sythelib-api-docs/objects/item/) | Item in ammo slot |

### Example Response

```json

{
    "head": {
        "id": 9803,
        "amount": 1,
        "name": "Cooking hood",
        "slot": 1
    },
    "cape": {
        "id": 9801,
        "amount": 1,
        "name": "Cooking cape",
        "slot": 2
    },
    "amulet": {
        "id": 3857,
        "amount": 1,
        "name": "Games necklace(6)",
        "slot": 3
    },
    "weapon": {
        "id": 3101,
        "amount": 1,
        "name": "Rune claws",
        "slot": 4
    },
    "torso": {
        "id": -1,
        "amount": 0,
        "name": "null",
        "slot": 5
    },
    "shield": {
        "id": -1,
        "amount": 0,
        "name": "null",
        "slot": 6
    },
    "legs": {
        "id": -1,
        "amount": 0,
        "name": "null",
        "slot": 8
    },
    "gloves": {
        "id": -1,
        "amount": 0,
        "name": "null",
        "slot": 10
    },
    "boots": {
        "id": -1,
        "amount": 0,
        "name": "null",
        "slot": 11
    },
    "ring": {
        "id": 11982,
        "amount": 1,
        "name": "Ring of wealth (4)",
        "slot": 13
    },
    "ammo": {
        "id": -1,
        "amount": -1,
        "name": "null",
        "slot": -1
    }
}

```