

# Inventory

## /inventory

Fetch all the items in the player's inventory.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| items | [Item](/sythelib-api-docs/objects/item/) Array| The position in the Array is the position in the inventory (From Left to Right, Top to bottom) |


### Example Response

```json
{
    "items": [
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 0
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 1
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 2
        },
        {
            "id": 314,
            "amount": 23384,
            "name": "Feather",
            "slot": 3
        },
        {
            "id": 11323,
            "amount": 1,
            "name": "Barbarian rod",
            "slot": 4
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 5
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 6
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 7
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 8
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 9
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 10
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 11
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 12
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 13
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 14
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 15
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 16
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 17
        },
        {
            "id": 11328,
            "amount": 1,
            "name": "Leaping trout",
            "slot": 18
        },
        {
            "id": 11330,
            "amount": 1,
            "name": "Leaping salmon",
            "slot": 19
        },
        {
            "id": -1,
            "amount": 0,
            "name": "null",
            "slot": 20
        },
        {
            "id": 11332,
            "amount": 1,
            "name": "Leaping sturgeon",
            "slot": 21
        },
        {
            "id": 11328,
            "amount": 1,
            "name": "Leaping trout",
            "slot": 22
        },
        {
            "id": 11328,
            "amount": 1,
            "name": "Leaping trout",
            "slot": 23
        },
        {
            "id": 11332,
            "amount": 1,
            "name": "Leaping sturgeon",
            "slot": 24
        },
        {
            "id": 11330,
            "amount": 1,
            "name": "Leaping salmon",
            "slot": 25
        },
        {
            "id": 11330,
            "amount": 1,
            "name": "Leaping salmon",
            "slot": 26
        },
        {
            "id": 11328,
            "amount": 1,
            "name": "Leaping trout",
            "slot": 27
        }
    ]
}
```