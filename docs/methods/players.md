
# Players

## /players

Fetch all the players that could be loaded from the player's position.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [Player](/sythelib-api-docs/objects/player/) Array| Array containing all the [Player](/sythelib-api-docs/objects/player/)s loaded.|

### Example Response
```json
[
    {
        "localPlayer": false,
        "name": "Moridod",
        "id": 329,
        "pos": {
            "x": 2499,
            "y": 3507
        },
        "healthRatio": -1,
        "healthScale": -1,
        "level": 118,
        "interacting": "Fishing spot",
        "interactingID": 13416,
        "overhead": 0,
        "skulled": false,
        "equipment": {
            "head": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 0
            },
            "cape": {
                "id": 9790,
                "amount": 1,
                "name": "Construct. cape(t)",
                "slot": 1
            },
            "amulet": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 2
            },
            "weapon": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 3
            },
            "torso": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 4
            },
            "shield": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 5
            },
            "legs": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 6
            },
            "gloves": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 7
            },
            "boots": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 8
            },
            "ring": {
                "id": -1,
                "amount": -1,
                "name": "null",
                "slot": -1
            },
            "ammo": {
                "id": -1,
                "amount": -1,
                "name": "null",
                "slot": -1
            }
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            256,
                            198
                        ],
                        [
                            256,
                            208
                        ],
                        [
                            280,
                            208
                        ],
                        [
                            280,
                            200
                        ],
                        [
                            278,
                            198
                        ]
                    ]
                }
            ]
        }
    },
    {
        "localPlayer": false,
        "name": "Alpy700",
        "id": 478,
        "pos": {
            "x": 2499,
            "y": 3506
        },
        "healthRatio": -1,
        "healthScale": -1,
        "level": 79,
        "interacting": "Fishing spot",
        "interactingID": 13419,
        "overhead": 0,
        "skulled": false,
        "equipment": {
            "head": {
                "id": 13258,
                "amount": 1,
                "name": "Angler hat",
                "slot": 0
            },
            "cape": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 1
            },
            "amulet": {
                "id": 1712,
                "amount": 1,
                "name": "Amulet of glory(4)",
                "slot": 2
            },
            "weapon": {
                "id": 21028,
                "amount": 1,
                "name": "Dragon harpoon",
                "slot": 3
            },
            "torso": {
                "id": 13259,
                "amount": 1,
                "name": "Angler top",
                "slot": 4
            },
            "shield": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 5
            },
            "legs": {
                "id": 13260,
                "amount": 1,
                "name": "Angler waders",
                "slot": 6
            },
            "gloves": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 7
            },
            "boots": {
                "id": 13261,
                "amount": 1,
                "name": "Angler boots",
                "slot": 8
            },
            "ring": {
                "id": -1,
                "amount": -1,
                "name": "null",
                "slot": -1
            },
            "ammo": {
                "id": -1,
                "amount": -1,
                "name": "null",
                "slot": -1
            }
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            256,
                            209
                        ],
                        [
                            256,
                            220
                        ],
                        [
                            281,
                            220
                        ],
                        [
                            281,
                            212
                        ],
                        [
                            279,
                            209
                        ]
                    ]
                }
            ]
        }
    }]
    ```
