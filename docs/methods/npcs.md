
# NPCs


----
All requests can be filtered by ID and/or name, which will only return Entities with such ID and/or name.
### Input parameters
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| id | number | Filter by ID|
| name | string | Filter by name|

### Example Request
`/.../?id=10233&name=Banker`

----

## /npcs

Fetch all the npcs that could be loaded from the player's position.

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [NPC](/sythelib-api-docs/objects/npc/) Array| Array containing all the [NPC](/sythelib-api-docs/objects/npc/)s loaded.|

### Example Response

```json
[
    {
        "name": "Fishing spot",
        "combatLvl": 0,
        "healthRatio": -1,
        "healthScale": -1,
        "interacting": "",
        "pos": {
            "x": 2500,
            "y": 3510
        },
        "index": 13416,
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            264,
                            166
                        ],
                        [
                            264,
                            174
                        ],
                        [
                            274,
                            174
                        ],
                        [
                            274,
                            166
                        ]
                    ]
                }
            ]
        }
    },
    {
        "name": "Fishing spot",
        "combatLvl": 0,
        "healthRatio": -1,
        "healthScale": -1,
        "interacting": "",
        "pos": {
            "x": 2500,
            "y": 3506
        },
        "index": 13419,
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            264,
                            207
                        ],
                        [
                            264,
                            217
                        ],
                        [
                            274,
                            217
                        ],
                        [
                            274,
                            208
                        ],
                        [
                            273,
                            207
                        ]
                    ]
                }
            ]
        }
    }]
```


## /npcs/neareast

Fetch the closes object to a Point. If no Point is defined it will return the nearest to the Player.

### Input parameters (optional)
GET query containing:

|Field name| Type | Description|
|----------|-----|-------------|
| x | number | x coordinate of the Tile|
| y | number | y coordinate of the Tile|
| z | number | z coordinate of the Tile|


### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| None | [NPC](/sythelib-api-docs/objects/npc/) | [NPC](/sythelib-api-docs/objects/npc/)s nearest to player.|

### Example Request
`/npcs/nearest?x=2500&y=3510`


### Example Response

```json
    {
        "name": "Fishing spot",
        "combatLvl": 0,
        "healthRatio": -1,
        "healthScale": -1,
        "interacting": "",
        "pos": {
            "x": 2500,
            "y": 3510
        },
        "index": 13416,
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            264,
                            166
                        ],
                        [
                            264,
                            174
                        ],
                        [
                            274,
                            174
                        ],
                        [
                            274,
                            166
                        ]
                    ]
                }
            ]
        }
    },
```