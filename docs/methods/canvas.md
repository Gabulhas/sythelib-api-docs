# Canvas 

## /canvas (POST)

Calculates the Centroid point of a canvas.


### POST Body


|Field name| Type | Description|
|----------|-----|-------------|
| - | [Canvas](/sythelib-api-docs/objects/canvas/) | Clickbox/Canvas you want to calculate. |

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| x | number | x coordinate|
| y | number | y coordinate|

### Example Response
```json
{
    "x": 300,
    "y": 432,
}
```


## /canvas/random (POST)

Calculates a random point of a canvas.


### POST Body


|Field name| Type | Description|
|----------|-----|-------------|
| - | [Canvas](/sythelib-api-docs/objects/canvas/) | Clickbox/Canvas you want to calculate. |

### Response parameters
JSON object containing:

|Field name| Type | Description|
|----------|-----|-------------|
| x | number | x coordinate|
| y | number | y coordinate|

### Example Response
```json
{
    "x": 300,
    "y": 432,
}
```