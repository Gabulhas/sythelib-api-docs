# Skill

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| level | number | Level in a Skill |
| boostedLevel | number | Boosted Level in a Skill |
| xp | number | Total XP in a Skill |


### Example
```json
{
 "level": 50,
        "boostedLevel": 50,
        "xp": 101512.0
}
```