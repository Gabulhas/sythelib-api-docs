# Position

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| x | number | x coordinate|
| y | number | y coordinate|
| z | number | z coordinate|


### Example 
```json
        "pos": {
            "x": 2499,
            "y": 3507
            "z": 0
        },
```