# Ground Item

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| id            | number| [ID of the object](https://www.osrsbox.com/tools/object-search/)|
|quantity | number| Amount of Ground Items in the stack|
| name | string | name of the Game Object|
| pos | [Position](/sythelib-api-docs/objects/pos/) | Position of the Game Object |
| canvas | [Canvas](/sythelib-api-docs/objects/canvas/) | Clickbox of the Game Object |

### Example

```json
{
        "id": 11332,
        "quantity": 1,
        "name": "Leaping sturgeon",
        "pos": {
            "x": 2499,
            "y": 3507
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            465,
                            244
                        ],
                        [
                            473,
                            252
                        ],
                        [
                            474,
                            252
                        ],
                        [
                            482,
                            243
                        ],
                        [
                            474,
                            236
                        ],
                        [
                            473,
                            236
                        ]
                    ]
                }
            ]
        }
    }
``` 