# Item

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| id            | number| [ID of the item](https://www.osrsbox.com/tools/item-search/)|
|amount| number | Amount of items in the stack|
| name | string | name of the item |
| slot | number | slot (if there's any) where the item is located (Equipment or inventory)|

### Example
```json 
        {
            "id": 11323,
            "amount": 1,
            "name": "Barbarian rod",
            "slot": 4
        },
``` 