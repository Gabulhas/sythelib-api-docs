# GameObject

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| id            | number| [ID of the object](https://www.osrsbox.com/tools/object-search/)|
| name | string | name of the Game Object|
| pos | [Position](/sythelib-api-docs/objects/pos/) | Position of the Game Object |
| canvas | [Canvas](/sythelib-api-docs/objects/canvas/) | Clickbox of the Game Object |


### Example 

```json
    {
        "id": 10819,
        "name": "Willow",
        "pos": {
            "x": 2513,
            "y": 3559
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            120,
                            17
                        ],
                        [
                            119,
                            17
                        ],
                        [
                            119,
                            18
                        ],
                        [
                            119,
                            18
                        ],
                        [
                            119,
                            19
                        ],
                        [
                            119,
                            19
                        ],
                        [
                            119,
                            20
                        ],
                        [
                            119,
                            20
                        ],
                        [
                            119,
                            22
                        ],
                    ]
                }
            ]
        }
    }
```