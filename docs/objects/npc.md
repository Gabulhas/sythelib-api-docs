# Npc

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| name | string | name of the NPC|
| combatLvl | number | Combat Level of the NPC|
| pos | [Position](/sythelib-api-docs/objects/pos/) | Position of the NPC|
|  interacting | string | Name of the Actor which the NPC is interacting with|
| index | number | Index of the NPC |
| canvas | [Canvas](/sythelib-api-docs/objects/canvas/) | Clickbox of the NPC|
| animation | number | Current animation of the NPC|

### Example
```json
 {
        "name": "Guard",
        "combatLvl": 0,
        "healthRatio": -1,
        "healthScale": -1,
        "interacting": "",
        "pos": {
            "x": 2500,
            "y": 3506
        },
        "index": 13419,
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            264,
                            207
                        ],
                        [
                            264,
                            217
                        ],
                        [
                            274,
                            217
                        ],
                        [
                            274,
                            208
                        ],
                        [
                            273,
                            207
                        ]
                    ]
                }
            ]
        }
    }
```