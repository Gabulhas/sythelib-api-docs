# Player

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| localPlayer | boolean | is the Player the Local Player|
| name | string | name of the Player|
| id | number | ID of the Player |
| pos | [Position](/sythelib-api-docs/objects/pos/) | Position of the Player|
| healthRatio | number | Health Ratio of the Player |
| healthScale | number | Health Scale of the Player |
| level | number | Combat Level of the NPC|
|  interacting | string | Name of the Actor which the Player is interacting with|
|  interactingID | number | ID of the Actor which the Player is interacting with|
| overhead | number | Protection prayer being used by the Player |
| skulled | boolean | is the Player Skulled|
| equipment | Equipment Object | Equipment of the Player (Check [Equipment](/sythelib-api-docs/methods/equipment))
| canvas | [Canvas](/sythelib-api-docs/objects/canvas/) | Clickbox of the Player|

### Example
```json
{
        "localPlayer": false,
        "name": "Zezima",
        "id": 478,
        "pos": {
            "x": 2499,
            "y": 3506
        },
        "healthRatio": -1,
        "healthScale": -1,
        "level": 79,
        "interacting": "Fishing spot",
        "interactingID": 13419,
        "overhead": 0,
        "skulled": false,
        "equipment": {
            "head": {
                "id": 13258,
                "amount": 1,
                "name": "Angler hat",
                "slot": 0
            },
            "cape": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 1
            },
            "amulet": {
                "id": 1712,
                "amount": 1,
                "name": "Amulet of glory(4)",
                "slot": 2
            },
            "weapon": {
                "id": 21028,
                "amount": 1,
                "name": "Dragon harpoon",
                "slot": 3
            },
            "torso": {
                "id": 13259,
                "amount": 1,
                "name": "Angler top",
                "slot": 4
            },
            "shield": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 5
            },
            "legs": {
                "id": 13260,
                "amount": 1,
                "name": "Angler waders",
                "slot": 6
            },
            "gloves": {
                "id": -1,
                "amount": 1,
                "name": "null",
                "slot": 7
            },
            "boots": {
                "id": 13261,
                "amount": 1,
                "name": "Angler boots",
                "slot": 8
            },
            "ring": {
                "id": -1,
                "amount": -1,
                "name": "null",
                "slot": -1
            },
            "ammo": {
                "id": -1,
                "amount": -1,
                "name": "null",
                "slot": -1
            }
        },
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            256,
                            209
                        ],
                        [
                            256,
                            220
                        ],
                        [
                            281,
                            220
                        ],
                        [
                            281,
                            212
                        ],
                        [
                            279,
                            209
                        ]
                    ]
                }
            ]
        }
    }

```