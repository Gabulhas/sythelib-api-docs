# Canvas

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| polys | Polygons Array| Polygons of Canvas|

----
# Polygons

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| verts | Vertices Array| Vertices of a Polygon |

----
# Vertices 

## Atributes

| Atribute name | Type | Description|
|---------------|------|-----------|
| none | (number Array) Array| Vertices of a Polygon |


## Example 

```json
        "canvas": {
            "polys": [
                {
                    "verts": [
                        [
                            120,
                            17
                        ],
                        [
                            119,
                            17
                        ],
                        [
                            119,
                            18
                        ],
                        [
                            119,
                            18
                        ],
                        [
                            119,
                            19
                        ],
                        [
                            119,
                            19
                        ],
                        [
                            119,
                            20
                        ],
                        [
                            119,
                            20
                        ],
                    ]
                }
            ]
        }
```